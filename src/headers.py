import os
import panel as pn
from .stocks import stock_dict

sub_title1 = pn.pane.Markdown("""
## Appliquer des contraintes à votre portefeuille :
""")

sub_title2 = pn.pane.Markdown("""
Parmi les actions disponibles dans la colonne de gauche, selectionnez celles qui vous souhaitez en 
particulier garder ou bien celles que vous ayez déjà dans votre portefeuille.
""")

sub_title4 = pn.pane.Markdown("""
Pour chaque action sélectionnée, indiquez le nombre d'actions minimum que vous souhaitez avoir dans votre portefeuille.
""")

date_range_comment = pn.pane.Markdown("Avec l'aide des curseurs, sélectionnez une période pour analyse des cours des actions :")

amount_available_comment = pn.pane.Markdown("Indiquez ici le montant maximal de que vous souhaitez investir :")
amount_available = pn.widgets.Spinner(value=1000, step=1, start=0)


samples_comment = pn.pane.Markdown("""Indiquez le nombre de portefeuilles différents que vous voulez générer 
                                   (plus grand le nombre, plus longtemps l\'optimisation du portefeuille prendra) :""")
samples = pn.widgets.Select(name='Select', options=[100, 1000, 100000, 1000000])


criterium_comment = pn.pane.Markdown("""Indiquez quel critère voulez-vous utiliser pour optimiser votre portefeuille :""")
criterium = pn.widgets.Select(name='Select', options=['Maximiser le ratio de Sharpe', 'Maximiser le retour sur investissement', 'Minimiser le risque'])


empty_pane = pn.pane.GIF(width=100)
empty_gif = pn.pane.GIF(width=100)
waiting_gif = pn.pane.GIF(os.path.join(os.getcwd(), 'img', 'ajax-loader.gif'), width=100)

constraints_dict = pn.widgets.StaticText(name='', value='')
status_portfolio = pn.widgets.StaticText(name='', value='')
constraint_widgets = pn.Column()
stocks_selector = pn.widgets.CrossSelector(name='Actions', value=[], options=list(stock_dict.values()))
validate_button1 = pn.widgets.Button(name='Valider portefeuille !')
validate_button2 = pn.widgets.Button(name='Optimiser portefeuille !', disabled=True, button_type='success')

empty_holoviews = pn.pane.HoloViews()
portfolio_graph = pn.pane.HoloViews()
timeseries_graph = pn.pane.HoloViews()